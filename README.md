# SEAMI - Sustained Electro-Acustic Musical Instrument

Dump repository for the realisation of a Sustained Electro-Acustic Musical Instrument following the path of the [SEAM project](https://github.com/s-e-a-m).

## Boards
### Hints from Giorgio Nottoli

- [XCORE-200 MULTICHANNEL AUDIO PLATFORM](https://www.xmos.ai/download/USB-Audio-Product-Brief(2).pdf)
- [Embedded microchip SAM S70](https://www.microchip.com/en-us/solutions/displays/embedded-graphics-solutions/mcu-guided-selection-tool-for-graphics/sam-s70-series#)
- [The JTAG interface](https://www.jtag.com/the-many-faces-of-the-jtag-interface-port/)
- [XK-AUDIO-216-MC-AB](https://www.digikey.it/en/products/detail/xmos/XK-AUDIO-216-MC-AB/5267340)

## Useful links from other projects

- [DIY MIDI Controller with motorized fader](https://www.reddit.com/r/synthdiy/comments/104qvea/diy_midi_controller_with_motorized_fader/)